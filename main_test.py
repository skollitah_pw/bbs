import unittest

import main


class TestBlumBlumShub(unittest.TestCase):

    def test_bbs_1(self):
        p, q = main.generate_p_and_q(2, 500)
        print(f'p={p}, q={q}, n={p * q}')
        seed = 54
        bbs1 = main.bbs_basic(p, q, seed)
        bbs2 = main.bbs_euler_theorem(p, q, seed)

        for i in range(100):
            self.assertEqual(next(bbs1), next(bbs2))

    def test_bbs_2(self):
        p, q = main.generate_p_and_q(2, 100)
        print(f'p={p}, q={q}, n={p * q}')
        seed = 17
        bbs1 = main.bbs_basic(p, q, seed)
        bbs2 = main.bbs_euler_theorem(p, q, seed)

        self.assertEqual([36, 31, 202, 71, 234], [next(bbs1) for i in range(5)])
        self.assertEqual([36, 31, 202, 71, 234], [next(bbs2) for i in range(5)])

    def test_bbs_3(self):
        p = 7
        q = 11
        seed = 5
        bbs1 = main.bbs_basic(p, q, seed)
        bbs2 = main.bbs_euler_theorem(p, q, seed)

        self.assertEqual([25, 9, 4, 16], [next(bbs1) for i in range(4)])
        self.assertEqual([25, 9, 4, 16], [next(bbs2) for i in range(4)])


if __name__ == '__main__':
    unittest.main()
