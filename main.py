def sieve_of_eratosthenes(min, max):
    is_prime = [True for i in range(max + 1)]
    p = 2
    while p * p <= max:
        if is_prime[p]:
            for i in range(p ** 2, max + 1, p):
                is_prime[i] = False
        p += 1

    is_prime[0] = False
    is_prime[1] = False

    return [p for p in range(max + 1) if is_prime[p] and p >= min]


def greatest_common_divisor(a, b):
    while b > 0:
        a, b = b, a % b
    return a


def lowest_common_multiplier(a, b):
    return int(a * b / greatest_common_divisor(a, b))


def bbs_basic(p, q, seed):
    n = p * q
    x = seed

    while True:
        x = pow(x, 2, n)
        yield x


def bbs_euler_theorem(p, q, seed):
    n = p * q
    i = 1

    while True:
        carmichael = pow(2, i, lowest_common_multiplier(p - 1, q - 1))
        x = pow(seed, carmichael, n)
        i += 1
        yield x


def generate_p_and_q(prime_min, prime_max):
    primes = [prime for prime in sieve_of_eratosthenes(prime_min, prime_max)]
    candidates = [prime for prime in primes if prime % 4 == 3]
    sophie_germain_primes = list()
    for prime in candidates:
        try:
            if primes.index(2 * prime + 1):
                sophie_germain_primes.append(prime)
        except Exception:
            continue

    length = len(sophie_germain_primes)
    return sophie_germain_primes[length - 2], sophie_germain_primes[length - 1]


if __name__ == '__main__':
    try:
        p_max = int(input("Max prime: "))
        seed = int(input("Seed: "))
        count = int(input("Number of random integers: "))
        p, q = generate_p_and_q(2, p_max)
        print(f'p={p}, q={q}, n={p * q}')

        bbs = bbs_basic(p, q, seed)
        random_ints = [next(bbs) for i in range(count)]

        print(f'Random ints: {random_ints}')
    except Exception as e:
        raise e
